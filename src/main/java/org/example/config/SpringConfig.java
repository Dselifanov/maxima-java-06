package org.example.config;


import org.example.services.TransportFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "org.example")
@Configuration
public class SpringConfig {

    @Bean
    public TransportFactory transportFactory(){
        return new TransportFactory();
    }
}
