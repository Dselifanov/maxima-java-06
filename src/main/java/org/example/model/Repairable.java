package org.example.model;

public interface Repairable {
    void startRepair();
    void finishRepair();
    boolean isRepairing();

}
