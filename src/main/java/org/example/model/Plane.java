package org.example.model;

public class Plane extends Transport {


    public Plane(String name, int capacity, int speed, float costOfKm ) {
        super(name, capacity, speed, costOfKm );
    }


   @Override
   public int getPrice(City city) {
       int costOfTrans;
       if (!city.hasAirport() )
           return 0;

        costOfTrans = (int) (this.getCostOfKm() * city.getDistanceKm());

       return costOfTrans;
   }


    }

