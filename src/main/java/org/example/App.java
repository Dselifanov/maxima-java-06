package org.example;


import org.example.config.SpringConfig;
import org.example.model.*;
import org.example.services.Logistics;
import org.example.services.TransportFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main( String[] args )
    {


        City moscow = new City("Moscow", 1200, true, true);


        Truck ford = new Truck("Ford", 300, 160, 160f);

        Truck scania = new Truck("Скания", 1000, 180, 175f);

        Ship blackPearl = new Ship("Черная жемчужина", 40000, 300, 260f);

        Plane boingPlane = new Plane("Боинг", 500,1000, 650f);


        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        Logistics logistics1 = context.getBean(Logistics.class);

        TransportFactory transportFactory = context.getBean(TransportFactory.class);

       Logistics logistics = new Logistics (ford, scania, blackPearl, boingPlane);

       System.out.println(logistics.getShipping(moscow, 250, 21).getName());


      // System.out.println(TransportFactory.getTransport(moscow, 13150, 8));


    }
}
