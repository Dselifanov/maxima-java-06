package org.example.services;


import org.example.model.City;
import org.example.model.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Logistics {

    @Autowired TransportFactory transportFactory;
    private Transport[] vehicles;
    private int weight, hours;
    private City city;

    @Autowired
    public Logistics(Transport... vehicles) {
        this.vehicles = vehicles;
    }
    boolean posibleDelivery;

    public Transport[] getVehicles() {
        return vehicles;
    }

    public Logistics(City city, int weight, int hours) {
        this.weight = weight;
        this.hours = hours;
        this.city = city;
    }

    public int getWeight() {
        return weight;
    }

    public int getHours() {
        return hours;
    }

    public City getCity() {
        return city;
    }



    public Transport getShipping(City city, int weight, int hours) {
        /*this.city=city;
        this.weight=weight;
        this.hours = hours;*/

        int costFact;
        int costMin=Integer.MAX_VALUE;
        Transport lowCostTrans = null;


        for (Transport transport: vehicles) {
            costFact = transport.getPrice(city);
            posibleDelivery = isShippingAvailable(city, weight, hours, transport);

            if (posibleDelivery && costMin > costFact) {
                costMin = costFact;
                lowCostTrans = transport;
            }
        }
        if (lowCostTrans==null){
            lowCostTrans = transportFactory.getTransport(city,weight,hours);
        }
       // System.out.println(costMin);
        return lowCostTrans;
    }
    public boolean isShippingAvailable(City city, int weight, int hours, Transport transport){
        posibleDelivery = transport.getPrice(city) !=0 && city.getDistanceKm() / transport.getSpeed() <= hours &&
                transport.getCapacity() >= weight && !(transport.isRepairing());
        if(transport.isRepairing()){
            System.out.println("Этот транспорт на ремонте");
        }
        return posibleDelivery;
    }


}
